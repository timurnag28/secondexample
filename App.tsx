import React, {Component} from 'react';
import {Text, TouchableOpacity} from 'react-native';

export default class App extends Component {
  showAlert(item: string) {
    alert(item);
  }
  render() {
    return (
      <TouchableOpacity
        onPress={() => {
          this.showAlert('22323');
        }}>
        <Text>Read the docs to discover what to do next:</Text>
      </TouchableOpacity>
    );
  }
}
